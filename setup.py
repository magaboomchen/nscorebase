import sys

if sys.version > '3':
    req_filename = "./requirements.txt"
else:
    raise ValueError("Can not support python 2!")

from setuptools import find_packages, setup

REQUIRES = []
try:
    with open(req_filename, "rb") as f:
        REQUIRES = [line.strip() for line in f.read().decode("utf-8").split("\n")]
        REQUIRES = [line for line in REQUIRES if "#" not in line]
except FileNotFoundError as myEx:
    raise Exception(myEx)

setup(
    name="nscorebase",
    version="0.0.1",
    author="Mike Chen;",
    author_email="magaboomchen@163.com",
    url="http://git@gitee.com:magaboomchen/nscorebase.git",
    license="MIT",
    install_requires=REQUIRES,
    description="National key research and development project",
    packages=find_packages(),
    long_description="Thanks for all authors!",
    classifiers=[
        "License :: OSI Approved :: MIT License",
        "Programming Language :: Python",
        "Programming Language :: Python :: 3.6.9",
        "Programming Language :: Python :: 3.7.2",
        "Programming Language :: Python :: 3.8.1",
        "Programming Language :: Python :: 3.9",
        "Programming Language :: Python :: 3.10",
        "Programming Language :: Python :: Implementation :: CPython",
        "Programming Language :: Python :: Implementation :: PyPy",
    ],
    keywords="Network Function Virtulization",
    python_requires=""">=3.6, 
                    !=3.6.0, !=3.6.1, !=3.6.2, !=3.6.3, !=3.6.4, !=3.6.5,
                    !=3.6.6,  !=3.6.7, !=3.6.8, !=3.7.1, !=3.8.0""",
)
