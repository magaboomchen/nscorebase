import logging
import subprocess

import psutil

from nscorebase.compatibility import x2str
from nscorebase.logcfg import LoggerConfigurator


class ShellProcessor(object):
    def __init__(self):
        logging.get_logger("psutil").setLevel(logging.ERROR)
        log_cfgr = LoggerConfigurator(__name__, './log',
            'shellProcessor.log', level='warning')
        self.logger = log_cfgr.get_logger()

    def listRunningProcess(self):
        self.logger.info("List running process.")
        for p in psutil.process_iter(attrs=['pid', 'name']):
            self.logger.info(p)

    def isProcessRun(self,processName):
        for p in psutil.process_iter(attrs=['pid', 'name']):
            if processName in p.info['name']:
                self.logger.info(processName + " has already running.")
                return True
        return False

    def runProcess(self, filepath, root=False):
        if root == True:
            user = "sudo "
        else:
            user = ""
        out_bytes = subprocess.check_output(
            [ user + filepath], shell=True)

    def getProcessCPUAndMemoryUtilization(self, pid, interval=1):
        p = psutil.Process(pid)
        cpuUtilList = p.cpu_percent(interval=interval)
        memoryUtilList = p.memory_info().rss
        return cpuUtilList, memoryUtilList

    def killProcess(self,processName):
        for p in psutil.process_iter(attrs=['pid', 'name']):
            if processName in p.info['name']:
                pid = int(p.info['pid'])
                out_bytes = subprocess.check_output(
                    ["sudo kill " + str(pid)], shell=True)

    def isPythonScriptRun(self,moduleName):
        for p in psutil.process_iter(attrs=['pid', 'name', 'cmdline']):
            self.logger.info(p)
            if p.info['name'] == "python":
                for cmdline in p.info['cmdline']:
                    if cmdline.count(moduleName) > 0:
                        return True
        return False

    def runPythonScript(self, filepath, root=False, cmdPrefix="", cmdSuffix="", enablepipe=False, enableuniversal=False):
        if root == True:
            user = 'sudo env "PATH=$PATH" '
        else:
            user = ""
        cmd = " {0} python {1} {2}".format(cmdPrefix, filepath, cmdSuffix)
        if enablepipe:
            return subprocess.Popen(
                [ user + cmd],
                stdin=subprocess.PIPE,
                stdout=subprocess.PIPE,
                stderr=subprocess.PIPE,
                universal_newlines=enableuniversal,
                shell=True
            )
        else:
            return subprocess.Popen(
                [ user + cmd],
                universal_newlines=enableuniversal,
                shell=True
            )

    def getPythonScriptProcessPid(self, scriptName):
        for p in psutil.process_iter(attrs=['pid', 'name', 'cmdline']):
            if p.info['name'] == "python":
                cmdline = " ".join(p.info['cmdline'])
                self.logger.debug("cmdline:{0}".format(cmdline))
                self.logger.debug("scriptName:{0}".format(scriptName))
                if cmdline.find(scriptName) != -1:
                    self.logger.debug(p.info['pid'])
                    return p.info['pid']
        return None

    def killPythonScript(self,moduleName):
        for p in psutil.process_iter(attrs=['pid', 'name', 'cmdline']):
            if p.info['name'] == "python":
                for cmdline in p.info['cmdline']:
                    if cmdline.find(moduleName) != -1:
                        pid = int(p.info['pid'])
                        out_bytes = subprocess.check_output(
                            ["sudo kill " + str(pid)], shell=True)
    
    def runShellCommand(self,shellCmd):
        res = subprocess.check_output([shellCmd], shell=True)
        return x2str(res)