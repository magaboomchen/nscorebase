import os

from nscorebase.errorhandler import ExceptionProcessor
from nscorebase.logcfg import LoggerConfigurator


def mkdirs(dir_path: str):
    log_cfgr = LoggerConfigurator("mkdirs", './log',
        'mkdirs.log', level='debug')
    logger = log_cfgr.get_logger()
    try:
        os.makedirs(dir_path)
    except Exception as ex:
        if str(ex).find("17") == -1:
            ExceptionProcessor(logger).log_exception(ex)
