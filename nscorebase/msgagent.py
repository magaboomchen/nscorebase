#!/usr/bin/python
# -*- coding: UTF-8 -*-

import sys

from packaging import version

from nscorebase.logcfg import LoggerConfigurator
from nscorebase.msgagtcfg.grpc.agt import GrpcAgent
from nscorebase.msgagtcfg.const import *
from nscorebase.msgagtcfg.rbmq.agt import RabbitMQAgent
from nscorebase.msgagtcfg.msg import SAMMessage


class MessageAgent(GrpcAgent, RabbitMQAgent):
    def __init__(self, *args, **kwargs):
        super(MessageAgent, self).__init__(*args, **kwargs)

    def __del__(self):
        self.log_cfgr = LoggerConfigurator(__name__, None,
                                              None, level='info')
        self.logger = self.log_cfgr.get_logger()
        self.logger.info("Delete MessageAgent.")
        for srcQueueName, thread in self._threadSet.items():
            self.logger.debug("check thread is alive?")
            if version.parse(sys.version.split(' ')[0]) \
                    >= version.parse('3.9'):
                threadLiveness = thread.is_alive()
            else:
                threadLiveness = thread.isAlive()
            if threadLiveness:
                self.logger.info("Kill thread: %d" % thread.ident)
                self._async_raise(thread.ident, KeyboardInterrupt)
                thread.join()

        self.logger.info("Disconnect from RabbiMQServer.")
        self._disConnectRabbiMQServer()

        if sys.version > '3':
            self.logger.info("Bugs: Unimplement gRPC server "
                             "stop function because of the unlimited wait time.")
            # for server in self.gRPCServersList:
            #     server.stop(None)
        else:
            self.logger.info("stop gRPC servers")
            for server in self.gRPCServersList:
                server.wait_for_termination()