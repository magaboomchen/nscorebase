from typing import Any, Dict, Union
from uuid import UUID

REQUEST_STATE_INITIAL = "REQUEST_STATE_INITIAL"
REQUEST_STATE_IN_PROCESSING = "REQUEST_STATE_IN_PROCESSING"
REQUEST_STATE_SUCCESSFUL = "REQUEST_STATE_SUCCESSFUL"
REQUEST_STATE_FAILED = "REQUEST_STATE_FAILED"
REQUEST_STATE_REJECT = "REQUEST_STATE_REJECT"
REQUEST_STATE_TIMEOUT = "REQUEST_STATE_TIMEOUT"


class Request(object):
    def __init__(self, user_id: int,
                 rq_id: UUID,
                 rq_type: str,
                 rq_src_q: str = None,
                 rq_src: Dict[str, Any] = None,
                 rq_state: Union[REQUEST_STATE_INITIAL, REQUEST_STATE_SUCCESSFUL] = REQUEST_STATE_INITIAL,
                 attributes: Dict[str, Any] = None
                 ):
        self.user_id = user_id  # 0 is root
        self.rq_id = rq_id
        self.rq_type = rq_type
        self.rq_src_q = rq_src_q
        self.rq_src = rq_src
        # e.g. {"srcIP": "10.0.0.1", "srcPort": 50001}
        self.rq_state = rq_state
        self.attributes = attributes
        # e.g. {'sfc':sfc, 'error':error}

    def __str__(self):
        string = "{0}\n".format(self.__class__)
        for key, values in self.__dict__.items():
            string = string + "{0}:{1}\n".format(key, values)
        return string

    def __repr__(self):
        return str(self)


class Reply(object):
    def __init__(self, rq_id: UUID,
                 rq_state: Union[REQUEST_STATE_INITIAL, REQUEST_STATE_SUCCESSFUL],
                 attributes: Dict[str, Any] = None
                 ):
        self.rq_id = rq_id
        self.rq_state = rq_state
        self.attributes = attributes

    def __str__(self):
        string = "{0}\n".format(self.__class__)
        for key, values in self.__dict__.items():
            string = string + "{0}:{1}\n".format(key, values)
        return string

    def __repr__(self):
        return str(self)
