#!/usr/bin/python
# -*- coding: UTF-8 -*-

from nscorebase.logcfg import LoggerConfigurator
from nscorebase.server import SERVER_TYPE_NORMAL, Server

# TODO: refactor, pytest


class ServerTester(object):
    def __init__(self, controlIfName):
        log_cfgr = LoggerConfigurator(__name__, './log',
                                         'databaseAgent.log', level='info')
        self.logger = log_cfgr.get_logger()

        server = Server(controlIfName, "192.168.122.222", SERVER_TYPE_NORMAL)

        server.updateIfSet()

        ifset = server.getIfSet()
        self.logger.info(ifset)

        server.printIfSet()

        server.updateControlNICMAC()

        controlNICMac = server.getControlNICMac()
        self.logger.info(controlNICMac)

        server.updateDataPathNICMAC()

        datapathNICMac = server.getDatapathNICMac()
        self.logger.info(datapathNICMac)

        server.printCpuUtil()


if __name__ == "__main__":
    controlIfName = "eno1"
    ServerTester(controlIfName)
