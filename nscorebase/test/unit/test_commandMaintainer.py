import uuid

import pytest

from nscorebase.command import (CMD_STATE_FAIL, CMD_STATE_SUCCESSFUL,
                              CMD_STATE_WAITING, CMD_TYPE_ADD_SFCI,
                              CMD_TYPE_DEL_SFCI, Command, CommandReply)
from nscorebase.cmdbase import CommandBase
from nscorebase.messageAgent import (MSG_TYPE_CLASSIFIER_CONTROLLER_CMD,
                                   MSG_TYPE_SERVER_MANAGER_CMD)


class TestCommandMaintainerClass(object):
    def setup_method(self, method):
        """ setup any state tied to the execution of the given method in a
        class.  setup_method is invoked for every test method of a class.
        """
        self.cm = CommandBase()

        self.cmd_id = uuid.uuid1()
        self.sfcUUID = uuid.uuid1()
        self.cmd = Command(CMD_TYPE_ADD_SFCI,self.cmd_id,attributes={'sfcUUID':self.sfcUUID})

        self.cCmdID = uuid.uuid1()
        self.cCmd = Command(CMD_TYPE_DEL_SFCI,self.cCmdID,attributes={'sfcUUID':self.sfcUUID})

        self.cCmdID2 = uuid.uuid1()
        self.cCmd2 = Command(CMD_TYPE_DEL_SFCI,self.cCmdID2,attributes={'sfcUUID':self.sfcUUID})

        self.cmdRply = CommandReply(self.cCmdID, CMD_STATE_SUCCESSFUL)

    def teardown_method(self, method):
        """ teardown any state that was previously setup with a setup_method
        call.
        """
        self.cm = None
        self.cmd = None

    @pytest.fixture(scope="function")
    def setup_addCmdandcCmd(self):
        # setup
        # add a parent command
        self.cm._commandsInfo[self.cmd_id] = {}
        self.cm._commandsInfo[self.cmd_id]['cmd'] = self.cmd
        self.cm._commandsInfo[self.cmd_id]['state'] = CMD_STATE_WAITING
        # add a child command
        self.cm._commandsInfo[self.cCmdID] = {}
        self.cm._commandsInfo[self.cCmdID]['cmd'] = self.cCmdID
        self.cm._commandsInfo[self.cCmdID]['state'] = CMD_STATE_WAITING
        self.cm._commandsInfo[self.cCmdID]['parentCmdID'] = self.cmd_id
        # add another child command
        self.cm._commandsInfo[self.cCmdID2] = {}
        self.cm._commandsInfo[self.cCmdID2]['cmd'] = self.cCmdID2
        self.cm._commandsInfo[self.cCmdID2]['state'] = CMD_STATE_WAITING
        self.cm._commandsInfo[self.cCmdID2]['parentCmdID'] = self.cmd_id
        # add child commands to parent command
        self.cm._commandsInfo[self.cmd_id]['childCmdID'] = {
            MSG_TYPE_CLASSIFIER_CONTROLLER_CMD:self.cCmdID,
            MSG_TYPE_SERVER_MANAGER_CMD:self.cCmdID2
            }
        yield
        # teardown

    @pytest.fixture(scope="function")
    def setup_addCmdRply(self):
        # setup
        self.cm._commandsInfo[self.cCmdID]['cmdReply'] = self.cmdRply
        self.cm._commandsInfo[self.cCmdID2]['cmdReply'] = self.cmdRply
        yield
        # teardown

    @pytest.fixture(scope="function")
    def setup_successfulcCmd(self):
        # setup
        self.cm._commandsInfo[self.cCmdID]['state'] = CMD_STATE_SUCCESSFUL
        self.cm._commandsInfo[self.cCmdID2]['state'] = CMD_STATE_SUCCESSFUL
        yield
        # teardown

    @pytest.fixture(scope="function")
    def setup_failedcCmd(self):
        # setup
        self.cm._commandsInfo[self.cCmdID]['state'] = CMD_STATE_FAIL
        yield
        # teardown

    def test_addCmd(self):
        self.cm.add_cmd(self.cmd)
        assert self.cm._commandsInfo[self.cmd_id]['cmd'] == self.cmd

    def test_delCmdwithChildCmd(self, setup_addCmdandcCmd):
        self.cm.del_cmd_with_child_cmd(self.cmd_id)
        assert self.cm._commandsInfo == {}

    def test_addChildCmd2Cmd(self, setup_addCmdandcCmd):
        self.cm.add_child_cmd_to_cmd(self.cmd_id, MSG_TYPE_CLASSIFIER_CONTROLLER_CMD, self.cCmdID)
        assert self.cm._commandsInfo[self.cmd_id]['childCmdID']\
            [MSG_TYPE_CLASSIFIER_CONTROLLER_CMD] == self.cCmdID

    def test_delChildCmd4Cmd(self,setup_addCmdandcCmd):
        self.cm.delChildCmd4Cmd(self.cmd_id,MSG_TYPE_CLASSIFIER_CONTROLLER_CMD)
        childCmdKey = self.cm._commandsInfo[self.cmd_id]['childCmdID'].keys()
        assert not MSG_TYPE_CLASSIFIER_CONTROLLER_CMD in childCmdKey

    def test_getCmdState(self,setup_addCmdandcCmd):
        state = self.cm.getCmdState(self.cmd_id)
        assert state == CMD_STATE_WAITING

    def test_changeCmdState(self,setup_addCmdandcCmd):
        self.cm.changeCmdState(self.cmd_id,CMD_STATE_SUCCESSFUL)
        state = self.cm._commandsInfo[self.cmd_id]['state']
        assert  state == CMD_STATE_SUCCESSFUL
    
    def test_addParentCmd2Cmd(self,setup_addCmdandcCmd):
        self.cm.addParentCmd2Cmd(self.cCmdID,self.cmd_id)
        assert self.cm._commandsInfo[self.cCmdID]['parentCmdID'] == self.cmd_id
    
    def test_getChildCmdState(self,setup_addCmdandcCmd):
        state = self.cm.getChildCmdState(self.cmd_id,MSG_TYPE_CLASSIFIER_CONTROLLER_CMD)
        assert state == CMD_STATE_WAITING
    
    def test_getCmdType(self,setup_addCmdandcCmd):
        assert self.cm.getCmdType(self.cmd_id) == CMD_TYPE_ADD_SFCI
    
    def test_getParentCmdID(self,setup_addCmdandcCmd):
        assert self.cm.getParentCmdID(self.cCmdID) == self.cmd_id
    
    def test_getCmd(self,setup_addCmdandcCmd):
        assert self.cm.get_cmd(self.cmd_id) == self.cmd
    
    def test_addCmdRply(self,setup_addCmdandcCmd):
        self.cm.addCmdRply(self.cCmdID,self.cmdRply)
        assert self.cm._commandsInfo[self.cCmdID]['cmdReply'] == self.cmdRply
    
    def test_getChildCMdRplyList(self,setup_addCmdandcCmd,setup_addCmdRply):
        ccrl = self.cm.getChildCMdRplyList(self.cmd_id)
        assert ccrl == [self.cmdRply,self.cmdRply]
    
    def test_isParentCmdSuccessful_Case1(self,setup_addCmdandcCmd):
        assert self.cm.isParentCmdSuccessful(self.cmd_id) == False
    
    def test_isParentCmdSuccessful_Case2(self,setup_addCmdandcCmd,
        setup_successfulcCmd):
        assert self.cm.isParentCmdSuccessful(self.cmd_id) == True
    
    def test_isParentCmdFailed_Case1(self,setup_addCmdandcCmd):
        assert self.cm.isParentCmdFailed(self.cmd_id) == False
    
    def test_isParentCmdFailed_Case2(self,setup_addCmdandcCmd,
        setup_failedcCmd):
        assert self.cm.isParentCmdFailed(self.cmd_id) == True
    
    def test_isParentCmdWaiting(self,setup_addCmdandcCmd):
        assert self.cm.isParentCmdWaiting(self.cmd_id) == True
    
    def test_isParentCmdWaiting(self,setup_addCmdandcCmd,
        setup_successfulcCmd):
        assert self.cm.isParentCmdWaiting(self.cmd_id) == False