import time

from nscorebase.logcfg import LoggerConfigurator
from nscorebase.messageAgent import MessageAgent, SAMMessage
from nscorebase.msgagtcfg.rpccfg import TEST_PORT

SENDER_WORKER_NUM = 1
MAX_RECEIVE_NUM = 100


class TestMessageAgentClass(object):
    def setup_method(self, method):
        """ setup any state tied to the execution of the given method in a
        class.  setup_method is invoked for every test method of a class.
        """
        self.mARecv = MessageAgent()
        self.mASend = MessageAgent(maxSenderNum=SENDER_WORKER_NUM)

        log_cfgr = LoggerConfigurator(__name__, './log',
                                         'databaseAgent.log', level='warning')
        self.logger = log_cfgr.get_logger()

    def teardown_method(self, method):
        """ teardown any state that was previously setup with a setup_method
        call.
        """

    # def test_isCommand(self):
    #     body = Command(1,2)
    #     assert self.mA.isCommand(body) == True
    #     body = 1
    #     assert self.mA.isCommand(body) == False

    # def test_isCommandReply(self):
    #     body = CommandReply(1,2)
    #     assert self.mA.isCommandReply(body) == True
    #     body = 1
    #     assert self.mA.isCommandReply(body) == False

    def test_requestMsgByRPC(self):
        self.mARecv.startMsgReceiverRPCServer("127.0.0.1", 49998)
        msg = {"a": 1, "b": 2, "c": 3, "d": 4}
        samMsg = SAMMessage("Test", msg)
        self.mASend.startMsgReceiverRPCServer("127.0.0.1", TEST_PORT)
        time.sleep(2)
        t1 = time.time()
        cnt = 0
        while cnt < SENDER_WORKER_NUM:
            self.mASend.sendMsgByRPC("127.0.0.1", 49998, samMsg)
            cnt += 1
        cnt = 0
        msgCnt = 0
        time.sleep(1)
        while cnt < MAX_RECEIVE_NUM:
            cnt += 1
            newMsg = self.mARecv.getMsgByRPC("127.0.0.1", 49998)
            if newMsg.getbody() != None:
                t2 = time.time()
                self.logger.info("time is {0}".format(t2-t1))
                body1 = newMsg.getbody()
                body2 = samMsg.getbody()
                assert body1 == body2
                msgCnt += 1
                if msgCnt == SENDER_WORKER_NUM:
                    break
            time.sleep(0.1)
        assert cnt != MAX_RECEIVE_NUM

    def test_requestMsgByRabbitMQ(self):
        self.mARecv.startRecvMsg("TEST_RECV_QUEUE")
        msg = {"a": 1, "b": 2, "c": 3, "d": 4}
        samMsg = SAMMessage("Test", msg)
        self.mASend.startRecvMsg("TEST_SEND_QUEUE")
        time.sleep(2)
        t1 = time.time()
        self.mASend.sendMsg("TEST_RECV_QUEUE", samMsg)
        while True:
            newMsg = self.mARecv.getMsg("TEST_RECV_QUEUE")
            if newMsg != None:
                t2 = time.time()
                self.logger.info("time is {0}".format(t2-t1))
                body1 = newMsg.getbody()
                body2 = samMsg.getbody()
                assert body1 == body2
                break
