import os
import subprocess

import pytest

from nscorebase.shellagt import ShellProcessor
from nscorebase.test.unit.fixtures import scripts
from nscorebase.orchestration import orchestrator

MANUAL_TEST = True


class TestShellProcessorClass(object):
    def setup_method(self, method):
        """ setup any state tied to the execution of the given method in a
        class.  setup_method is invoked for every test method of a class.
        """
        self.sP = ShellProcessor()

    def teardown_method(self, method):
        """ teardown any state that was previously setup with a setup_method
        call.
        """
        self.sP = None

    def test_isProcessRun(self):
        assert self.sP.isProcessRun("bioset") == True

    @pytest.mark.skipif(MANUAL_TEST == True, reason='Manual testing is required')
    def test_runProcess(self):
        pass

    @pytest.mark.skipif(MANUAL_TEST == True, reason='Manual testing is required')
    def test_killProcess(self):
        pass
    
    def test_runPythonScript(self):
        filepath = tmpScript.__file__
        self.sP.runPythonScript(filepath)
        out_bytes = subprocess.check_output(
            ["ps -ef | grep scripts.py"], shell=True)
        out_bytes = str(out_bytes)
        assert out_bytes.count("tmpScript") >= 3

    def test_isPythonScriptRun(self):
        filepath = tmpScript.__file__
        subprocess.Popen(
            ["python " + filepath], shell=True)
        assert self.sP.isPythonScriptRun("scripts.py") == True

    def test_killPythonScript(self):
        filepath = tmpScript.__file__
        self.sP.runPythonScript(filepath)
        self.sP.killPythonScript("scripts.py")
        out_bytes = subprocess.check_output(
            ["ps -ef | grep scripts.py"], shell=True)
        out_bytes = str(out_bytes)
        assert out_bytes.count("scripts.py") == 2

    def test_getPythonScriptProcessPid(self):
        orchestratorFilePath = os.path.abspath(orchestrator.__file__)
        cmdline = "python {0} -name 0-19 -p 20 -minPIdx 0 -maxPIdx 19".format(orchestratorFilePath)
        pId = self.sP.getPythonScriptProcessPid(cmdline)
        print("pId is {0}".format(pId))
        assert 2 == 2