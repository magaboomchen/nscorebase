from nscorebase.logcfg import LoggerConfigurator
from nscorebase.test.testBase import TestBase


class TestLoggerClass(TestBase):
    def setup_method(self, method):
        """ setup any state tied to the execution of the given method in a
        class.  setup_method is invoked for every test method of a class.
        """

        log_cfgr = LoggerConfigurator(__name__, './log',
            'Logger.log', level='info', multiprocess=True)
        self.logger = log_cfgr.get_logger()

    def teardown_method(self, method):
        """ teardown any state that was previously setup with a setup_method
        call.
        """

    def test_requestMsgByRPC(self):
        self.logger.debug("Hello")
        self.logger.info("Hello")
        self.logger.warning("Hello")
        self.logger.error("Hello")
        self.logger.critical('critic')
        assert True
