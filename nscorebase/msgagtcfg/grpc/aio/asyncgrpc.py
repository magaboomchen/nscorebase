'''
This asyncGrpcAgent is currently not work!
It's seems that we need to implement a remote coroutine to utilize asyncio.
Here is a possible solution: https://grpclib.readthedocs.io/en/latest/
'''

import queue as Queue
from json.encoder import INFINITY

import grpc

import nscorebase.msgagtcfg.grpc.messageAgent_pb2_grpc as messageAgent_pb2_grpc
from nscorebase.msgagtcfg.grpc.aio.grpcsender import \
    AsyncGrpcSender
from nscorebase.msgagtcfg.grpc.aio.storage import \
    AsyncMsgStorageServicer
from nscorebase.msgagtcfg.grpc.agtbase import GrpcAgentBase
from nscorebase.msgagtcfg.rpccfg import MAX_MESSAGE_LENGTH


class AsyncGrpcAgent(GrpcAgentBase):
    def __init__(self, *args, **kwargs):
        super(AsyncGrpcAgent, self).__init__(*args, **kwargs)
        assert self.maxSenderNum == 1   # Not support muliprocess now
        self.gs = AsyncGrpcSender()

    async def sendMsgByRPC(self, dstIP, dstPort, message, maxRetryNum=INFINITY, enableTimeDebug=False):
        assert isinstance(self.gs, AsyncGrpcSender)
        await self.gs.sendMsgByAsyncRPC(dstIP, dstPort, message, maxRetryNum, enableTimeDebug)

    async def startMsgReceiverRPCServer(self, listenIP, listenPort):
        self.setListenSocket(listenIP, listenPort)
        srcQueueName = "{0}:{1}".format(listenIP, listenPort)
        if srcQueueName in self.msgQueues:
            self.logger.warning("Already listening on recv socket.")
        else:
            self.msgQueues[srcQueueName] = Queue.Queue()
            server = grpc.aio.server(
                # futures.ThreadPoolExecutor(
                #     max_workers=MAX_GRPC_RECEIVE_WORKER),
                None,
                options=[
                    ('grpc.max_send_message_length', MAX_MESSAGE_LENGTH),
                    ('grpc.max_receive_message_length', MAX_MESSAGE_LENGTH),
                    ('grpc.keepalive_time_ms', 10000),
                    # send keepalive ping every 10 second, default is 2 hours
                    ('grpc.keepalive_timeout_ms', 5000),
                    # keepalive ping time out after 5 seconds, default is 20 seoncds
                    ('grpc.keepalive_permit_without_calls', True),
                    # allow keepalive pings when there's no gRPC calls
                    ('grpc.http2.max_pings_without_data', 0),
                    # allow unlimited amount of keepalive pings without data
                    ('grpc.http2.min_time_between_pings_ms', 10000),
                    # allow grpc pings from client every 10 seconds
                    ('grpc.http2.min_ping_interval_without_data_ms',  5000),
                    # allow grpc pings from client without data every 5 seconds
                    ("grpc.optimization_target", 'throughput'),
                    # ('grpc.enable_retries', 1),
                ],
            )
            messageAgent_pb2_grpc.add_MessageStorageServicer_to_server(
                AsyncMsgStorageServicer(self.msgQueues[srcQueueName]), server)

            self.logger.info(
                'Starting server. Listening on port {0}.'.format(listenPort))
            server.add_insecure_port('{0}:{1}'.format(listenIP, listenPort))
            await server.start()
            self.asyncGrpcServerList.append(server)

    async def delAllServers(self):
        self.logger.info("stop gRPC servers")
        for server in self.asyncGrpcServerList:
            await server.wait_for_termination()