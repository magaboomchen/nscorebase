import os
import time

os.environ['PYTHONASYNCIODEBUG'] = '1'
import asyncio

from nscorebase.msgagtcfg.aio.asyncagt import AsyncMessageAgent
from nscorebase.logcfg import LoggerConfigurator
from nscorebase.messageAgent import SAMMessage
from nscorebase.msgagtcfg.rpccfg import TEST_PORT

SENDER_WORKER_NUM = 1
MAX_RECEIVE_NUM = 100

class TestAsyncClient(object):
    def setup(self):
        self.mASend = AsyncMessageAgent()
        log_cfgr = LoggerConfigurator(__name__, './log',
                                         'TestAsyncClient.log', 
                                         level='debug')
        self.logger = log_cfgr.get_logger()

    def start_send(self):
        self.setup()
        loop = asyncio.get_event_loop()
        loop.run_until_complete(asyncio.wait([self.start_server()]))
        loop.close()

    async def start_server(self):
        # await self.mASend.startMsgReceiverRPCServer("127.0.0.1", TEST_PORT)
        # msg = {"a": 1, "b": 2, "c": 3, "d": 4}
        # samMsg = SAMMessage("Test", msg)
        # await self.mASend.sendMsgByRPC("127.0.0.1", 49998, samMsg)
        # self.logger.info("Send successfully.")

        msg = {"a": 1, "b": 2, "c": 3, "d": 4}
        samMsg = SAMMessage("Test", msg)
        await self.mASend.startMsgReceiverRPCServer("127.0.0.1", TEST_PORT)
        time.sleep(2)
        t1 = time.time()
        cnt = 0
        while cnt < SENDER_WORKER_NUM:
            await self.mASend.sendMsgByRPC("127.0.0.1", 49998, samMsg)
            cnt += 1
        cnt = 0
        msgCnt = 0
        time.sleep(1)
        while cnt < MAX_RECEIVE_NUM:
            cnt += 1
            newMsg = self.mASend.getMsgByRPC("127.0.0.1", 49998)
            if newMsg.getbody() != None:
                t2 = time.time()
                self.logger.info("time is {0}".format(t2-t1))
                body1 = newMsg.getbody()
                body2 = samMsg.getbody()
                self.logger.info("asserting")
                assert body1 == body2
                msgCnt += 1
                if msgCnt == SENDER_WORKER_NUM:
                    break
            time.sleep(0.1)
        assert cnt != MAX_RECEIVE_NUM

if __name__ == "__main__":
    tas = TestAsyncClient()
    tas.start_send()