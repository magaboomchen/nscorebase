#!/usr/bin/python
# -*- coding: UTF-8 -*-

import asyncio

import grpc

import nscorebase.msgagtcfg.grpc.messageAgent_pb2 as messageAgent_pb2
import nscorebase.msgagtcfg.grpc.messageAgent_pb2_grpc as messageAgent_pb2_grpc
from nscorebase.msgagtcfg.coder import MessageCoder


class RpcClient(object):
    # rpc_client = {}
    rpc_client = None

    @staticmethod
    def get_rpc_channel(host, port):
        RPC_OPTIONS = [('grpc.max_send_message_length', 100 * 1024 * 1024),
              ('grpc.max_receive_message_length', 100 * 1024 * 1024),
              ('grpc.enable_retries', 1),
              ('grpc.service_config',
               '{"retryPolicy": {"maxAttempts": 4, "initialBackoff": "0.1s", '
               '"maxBackoff": "1s", "backoffMutiplier": 2, '
               '"retryableStatusCodes": ["UNAVAILABLE"]}}'),
              ]
        options = RPC_OPTIONS
        channel = grpc.insecure_channel("{}:{}".format(host, port),
                                        options=options)
        return channel 

    def load_sub_rpc(self, host, port):
        """
        function return rpc instance
        :param platform
        :param host
        :param port
        :param db_type
        :return: instance
        """
        channel = self.get_rpc_channel(host, port)
        stub = messageAgent_pb2_grpc.AsyncMessageStorageStub(channel)
        return stub

    async def send(self, stub):
        message = {}
        pickles = MessageCoder.encodeMessage(message)
        pickles = bytes(pickles)
        req = messageAgent_pb2.Pickle(picklebytes=pickles)
        print("here")
        stub.Store(req)


if __name__ == "__main__":
    rc = RpcClient()
    stub = rc.load_sub_rpc("127.0.0.1", 50051)
    loop = asyncio.get_event_loop()
    loop.run_until_complete(asyncio.wait([rc.send(stub)]))
    loop.close()