#!/usr/bin/python
# -*- coding: UTF-8 -*-

import asyncio
import queue as Queue
from concurrent import futures

from grpc.experimental import aio

import nscorebase.msgagtcfg.grpc.messageAgent_pb2_grpc as messageAgent_pb2_grpc
from nscorebase.msgagtcfg.grpc.aio.storage import \
    AsyncMsgStorageServicer


async def start_server():
    # start rpc service
    server = aio.server(futures.ThreadPoolExecutor(max_workers=40), options=[
        ('grpc.so_reuseport', 0),
        ('grpc.max_send_message_length', 100 * 1024 * 1024),
        ('grpc.max_receive_message_length', 100 * 1024 * 1024),
        ('grpc.enable_retries', 1),
    ])
    queue = Queue.Queue()
    messageAgent_pb2_grpc.add_AsyncMessageStorageServicer_to_server(
                AsyncMsgStorageServicer(queue), server)
    
    server.add_insecure_port('[::]:50051')
    await server.start()

    # since server.start() will not block,
    # a sleep-loop is added to keep alive
    try:
        await server.wait_for_termination()
    except KeyboardInterrupt:
        await server.stop(None)

if __name__ == '__main__':
    loop = asyncio.get_event_loop()
    loop.run_until_complete(asyncio.wait([start_server()]))
    loop.close()