import itertools
import sys
import time
from typing import List, Tuple

if sys.version >= '3':
    import queue as Queue
else:
    import Queue

from concurrent import futures
from json.encoder import INFINITY

import grpc

import nscorebase.msgagtcfg.grpc.messageAgent_pb2_grpc as messageAgent_pb2_grpc
from nscorebase.msgagtcfg.grpc.agtbase import GrpcAgentBase
from nscorebase.msgagtcfg.grpc.sender import GrpcSender
from nscorebase.msgagtcfg.grpc.metadata import MsgMetadata
from nscorebase.msgagtcfg.grpc.storage import MsgStorageServicer
from nscorebase.msgagtcfg.rpccfg import (
    MAX_GRPC_RECEIVE_WORKER, MAX_MESSAGE_LENGTH)


class GrpcAgent(GrpcAgentBase):
    def __init__(self, *args, **kwargs):
        super(GrpcAgent, self).__init__(*args, **kwargs)
        self.gs = GrpcSender()
        self.gs.init()

    def startGRPCSenders(self):
        self.gRPCSenderProcessMetadataList = \
            []  # type: (List[Tuple[Queue.Queue, GrpcSender, int]])
        for idx in range(self.maxSenderNum):
            # construct shared queue
            mpQueue = self.mm.manager.Queue()
            gs = GrpcSender()
            # start multiple sender process
            assert self.listenIP != None
            pid = self.mm.start_process(
                gs, name="gs{0}".format(idx), listenIP=self.listenIP, listenPort=self.listenPort, mpQueue=mpQueue)
            self.gRPCSenderProcessMetadataList.append((mpQueue, gs, pid))
        self.senderRoundRobinSelector = itertools.cycle(
            self.gRPCSenderProcessMetadataList)  # type: itertools.cycle
        self.senderInit = True

    def sendMsgByRPC(self, dstIP, dstPort, message, maxRetryNum=INFINITY, enableTimeDebug=False):
        if self.enableMultiprocess:
            # self.logger.debug("message body is {0}".format(message.getbody()))
            t1 = time.time()
            assert self.listenIP != None and self.listenPort != None
            if not self.senderInit:
                self.logger.info("start grpc senders.")
                self.startGRPCSenders()
            # round robin msg to different process
            if sys.version > '3':
                senderMetaData = next(self.senderRoundRobinSelector)
            else:
                senderMetaData = self.senderRoundRobinSelector.next()
            t2 = time.time()
            mpQueue = senderMetaData[0]  # type: Queue.Queue
            msgMD = MsgMetadata(dstIP, dstPort, message,
                                maxRetryNum, enableTimeDebug)
            t3 = time.time()
            while True:
                if not mpQueue.full():
                    mpQueue.put_nowait(msgMD)
                    break
            t4 =time.time()
            if enableTimeDebug:
                self.logger.debug("send d1 {0}, d2 {1}, d3 {2}".format(t2-t1, t3-t2, t4-t3))
        else:
            # self.logger.debug("single process.")
            self.gs.sendMsgByRPC(dstIP, dstPort, message, maxRetryNum, enableTimeDebug)

    def startMsgReceiverRPCServer(self, listenIP, listenPort, msgBufferSize=9999):
        self.setListenSocket(listenIP, listenPort)
        srcQueueName = "{0}:{1}".format(listenIP, listenPort)
        if srcQueueName in self.msgQueues:
            self.logger.warning("Already listening on recv socket.")
        else:
            self.msgQueues[srcQueueName] = Queue.Queue()
            server = grpc.server(
                futures.ThreadPoolExecutor(
                    max_workers=MAX_GRPC_RECEIVE_WORKER),
                options=[
                    ('grpc.max_send_message_length', MAX_MESSAGE_LENGTH),
                    ('grpc.max_receive_message_length', MAX_MESSAGE_LENGTH),
                    ('grpc.keepalive_time_ms', 10000),
                    # send keepalive ping every 10 second, default is 2 hours
                    ('grpc.keepalive_timeout_ms', 5000),
                    # keepalive ping time out after 5 seconds, default is 20 seoncds
                    ('grpc.keepalive_permit_without_calls', True),
                    # allow keepalive pings when there's no gRPC calls
                    ('grpc.http2.max_pings_without_data', 0),
                    # allow unlimited amount of keepalive pings without data
                    ('grpc.http2.min_time_between_pings_ms', 10000),
                    # allow grpc pings from client every 10 seconds
                    ('grpc.http2.min_ping_interval_without_data_ms',  5000),
                    # allow grpc pings from client without data every 5 seconds
                    ("grpc.optimization_target", 'throughput'),
                ],
            )
            messageAgent_pb2_grpc.add_MessageStorageServicer_to_server(
                MsgStorageServicer(self.msgQueues[srcQueueName], msgBufferSize), server)

            self.logger.info(
                'Starting server. Listening on port {0}.'.format(listenPort))
            server.add_insecure_port('{0}:{1}'.format(listenIP, listenPort))
            server.start()
            self.gRPCServersList.append(server)
