import sys

if sys.version >= '3':
    import queue as Queue
else:
    import Queue

import socket
import time
from json.encoder import INFINITY

import grpc

import nscorebase.msgagtcfg.grpc.messageAgent_pb2 as messageAgent_pb2
import nscorebase.msgagtcfg.grpc.messageAgent_pb2_grpc as messageAgent_pb2_grpc
from nscorebase.errorhandler import ExceptionProcessor
from nscorebase.logcfg import LoggerConfigurator
from nscorebase.msgagtcfg.grpc.metadata import MsgMetadata
from nscorebase.msgagtcfg.cfg import (
    MESSAGE_AGENT_GRPC_ERROR_LOG_TIME_SLOT, MESSAGE_AGENT_GRPC_RETRY_WAIT_TIME)
from nscorebase.msgagtcfg.rpccfg import (MAX_MESSAGE_LENGTH,
                                         MAX_PORT, MIN_PORT)
from nscorebase.msgagtcfg.coder import MessageCoder
from nscorebase.msgagtcfg.msg import SAMMessage


class GrpcSender(object):
    def __init__(self):
        self.listenIP = None
        self.listenPort = None
        self.gRPCChannelDict = {}
        self.stubDict = {}

    def init(self):
        self.log_cfgr = LoggerConfigurator(__name__, './log',
                                              'GrpcSender.log',
                                              level='info',
                                              multiprocess=True)
        self.logger = self.log_cfgr.get_logger()
        self.logger.debug("GrpcSender is running.")

        self.gRPCChannelDict = {}
        self.stubDict = {}

    def __call__(self, listenIP, listenPort, mpQueue):
        # type: (str, int, Queue.Queue) -> None
        self.init()
        self.logger.info("listen ip is {0}".format(listenIP))
        self.listenIP = listenIP
        self.mpQueue = mpQueue
        if listenPort != None:
            self.listenPort = listenPort
        else:
            self.listenPort = GrpcSender.getOpenSocketPort()
        while True:
            msgMetadata = self.mpQueue.get(block=True, timeout=None)    # type: MsgMetadata
            dstIP = msgMetadata.dstIP
            dstPort = msgMetadata.dstPort
            message = msgMetadata.message
            maxRetryNum = msgMetadata.maxRetryNum
            enableTimeDebug = msgMetadata.enableTimeDebug
            self.sendMsgByRPC(dstIP, dstPort, message,
                                maxRetryNum, enableTimeDebug)

    def sendMsgByRPC(self, dstIP, dstPort, message, maxRetryNum=INFINITY,
                     enableTimeDebug=False):
        # type: (str, int, SAMMessage, int, bool) -> None
        # see following link to learn about async
        # https://grpc.github.io/grpc/python/grpc_asyncio.html
        if self.listenIP == None or self.listenPort == None:
            raise ValueError("Unset listen IP {0} or port {1}.".format(self.listenIP, self.listenPort))
        self.logger.debug(
            "gRPC send to dstIP {0}, dstPort {1}".format(dstIP, dstPort))
        # self.logger.debug("msg body is {0}".format(message.getbody()))
 
        cnt = 0
        while True:
            sendSucc = False
            try:
                t1 = time.time()
                self.establishGrpcChannel(dstIP, dstPort)
                t2 = time.time()

                if type(message) == SAMMessage:
                    source = {"comType": "RPC",
                              "srcIP": self.listenIP,
                              "srcPort": self.listenPort
                              }
                    message.setSource(source)
                pickles = MessageCoder.encodeMessage(message)
                pickles = bytes(pickles)
                t3 = time.time()
                req = messageAgent_pb2.Pickle(picklebytes=pickles)
                response = self.stubDict[(dstIP, dstPort)].Store(req)
                t4 = time.time()
                if enableTimeDebug:
                    self.logger.debug(
                        "\nd1 {0}\nd2 {1}\nd3 {2}".format(t2-t1, t3-t2, t4-t3))

                self.logger.debug("response is {0}".format(response))
                if response.booly:
                    sendSucc = True
                    break
            except grpc.RpcError as e:
                if cnt % MESSAGE_AGENT_GRPC_ERROR_LOG_TIME_SLOT == 0:
                    # ouch!
                    # lets print the gRPC error message
                    # which is "Length of `Name` cannot be more than 10 characters"
                    # self.logger.error(e.details())
                    # lets access the error code, which is `INVALID_ARGUMENT`
                    # `type` of `status_code` is `grpc.StatusCode`
                    status_code = e.code()
                    # should print `INVALID_ARGUMENT`
                    # self.logger.error(status_code.name)
                    # should print `(3, 'invalid argument')`
                    # self.logger.error(status_code.value)
                    self.logger.error("connecting socket {0}:{1} failed. "
                                      "details: {2}; "
                                      "statusCodeName: {3}; statusCodeValue: {4}".format(
                                          dstIP, dstPort,
                                          e.details(), status_code.name, status_code.value
                                      ))
                    # want to do some specific action based on the error?
                    if grpc.StatusCode.INVALID_ARGUMENT == status_code:
                        # do your stuff here
                        pass
            except Exception as ex:
                ExceptionProcessor(self.logger).log_exception(ex,
                                                             "messageAgent")
            finally:
                if not sendSucc:
                    cnt += 1
                    if cnt > maxRetryNum:
                        break
                    self.logger.info(
                        "retry for the {0}/{1} times".format(cnt, maxRetryNum))
                    time.sleep(MESSAGE_AGENT_GRPC_RETRY_WAIT_TIME)

    def establishGrpcChannel(self, dstIP, dstPort):
        # type: (str, int) -> None
        # enable reuseport in default setting
        # see following links to get more options of insecure_channel() methods.
        # https://github.com/grpc/grpc/blob/v1.46.x/include/grpc/impl/codegen/grpc_types.h
        if (dstIP, dstPort) in self.gRPCChannelDict.keys():
            return
        else:
            self.gRPCChannelDict[(dstIP, dstPort)] = grpc.insecure_channel(
                '{0}:{1}'.format(dstIP, dstPort),
                options=[
                    ('grpc.max_send_message_length', MAX_MESSAGE_LENGTH),
                    ('grpc.max_receive_message_length', MAX_MESSAGE_LENGTH),
                    ('grpc.keepalive_time_ms', 10000),
                    ('grpc.keepalive_timeout_ms', 5000),
                    ('grpc.keepalive_permit_without_calls', True),
                    ('grpc.http2.max_pings_without_data', 0),
                    ('grpc.http2.min_time_between_pings_ms', 10000),
                    ('grpc.http2.min_ping_interval_without_data_ms', 5000),
                    ("grpc.optimization_target", 'throughput'),
                ],
            )
            self.stubDict[(dstIP, dstPort)] = messageAgent_pb2_grpc.MessageStorageStub(
                channel=self.gRPCChannelDict[(dstIP, dstPort)])

    @staticmethod
    def getOpenSocketPort():
        while True:
            s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            s.bind(("", 0))
            s.listen(1)
            port = s.getsockname()[1]
            s.close()
            if port < MIN_PORT or port > MAX_PORT:
                return port

    def __del__(self):
        self.log_cfgr = LoggerConfigurator(__name__, None,
                                              None, level='info',
                                              multiprocess=False)
        self.logger = self.log_cfgr.get_logger()
        self.logger.info("close gRPC channel")
        for key, grpcChannel in self.gRPCChannelDict.items():
            if grpcChannel != None:
                grpcChannel.close()
