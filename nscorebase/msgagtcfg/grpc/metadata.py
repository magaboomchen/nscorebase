from typing import Any


class MsgMetadata(object):
    def __init__(self, dstIP, dstPort, message, maxRetryNum, enableTimeDebug):
        # type: (str, str, Any, int, bool) -> None
        self.dstIP = dstIP
        self.dstPort = dstPort
        self.message = message
        self.maxRetryNum = maxRetryNum
        self.enableTimeDebug = enableTimeDebug