import nscorebase.msgagtcfg.grpc.messageAgent_pb2 as messageAgent_pb2
import nscorebase.msgagtcfg.grpc.messageAgent_pb2_grpc as messageAgent_pb2_grpc
from nscorebase.msgagtcfg.cfg import \
    MESSAGE_AGENT_MAX_QUEUE_SIZE


class MsgStorageServicer(messageAgent_pb2_grpc.MessageStorageServicer):
    def __init__(self, msgQueue, msgBufferSize = MESSAGE_AGENT_MAX_QUEUE_SIZE):
        self.msgQueue = msgQueue
        self.msgBufferSize = msgBufferSize

    def Store(self, request, context):
        pickles = request.picklebytes
        if self.msgQueue.qsize() < self.msgBufferSize:
            self.msgQueue.put(pickles)
        else:
            raise ValueError("MessageAgent recv qeueu full! Drop new msg!")
        return messageAgent_pb2.Status(booly=True)
