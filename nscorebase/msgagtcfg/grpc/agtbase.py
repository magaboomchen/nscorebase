import sys
from typing import List

from grpc import Server

from nscorebase.msgagtcfg.grpc.sender import GrpcSender
from nscorebase.msgagtcfg.agtbase import MessageAgentBase


class GrpcAgentBase(MessageAgentBase):
    def __init__(self, *args, **kwargs):
        super(GrpcAgentBase, self).__init__(*args, **kwargs)
        self.listenIP = None
        self.listenPort = None
        self.gRPCServersList = []   # type: List[Server]
        if sys.version > '3':
            from grpc.aio import Server as AsyncServer
            self.asyncGrpcServerList = []   # type: List[AsyncServer]
        else:
            self.asyncGrpcServerList = []
        self.senderInit = False
        self.gs = GrpcSender()

    def setListenSocket(self, listenIP, listenPort):
        self.listenIP = listenIP
        self.listenPort = listenPort
        self.gs.listenIP = listenIP
        self.gs.listenPort = listenPort

    def getListenIP(self):
        return self.listenIP

    def getListenPort(self):
        return self.listenPort

    def getMsgByRPC(self, listenIP, listenPort, block=False, timeout=None):
        msg = self.getMsg("{0}:{1}".format(listenIP, listenPort), block, timeout)
        return msg
