#!/usr/bin/python
# -*- coding: UTF-8 -*-

from nscorebase.test.testConfig import TEST_TYPE, TEST_TYPE_INTEGRATE, TEST_TYPE_LOCAL

MAX_MESSAGE_LENGTH = int(2**31 - 1)
MAX_GRPC_RECEIVE_WORKER = 12

if TEST_TYPE == TEST_TYPE_LOCAL:
    TEST_IP = "127.0.0.1"
    TEST_PORT = 49999

    DASHBOARD_IP = "127.0.0.1"
    DASHBOARD_PORT = 50000

    MEASURER_IP = "127.0.0.1"
    MEASURER_PORT = 50001

    MEASURER_COLLECTOR_IP = "127.0.0.1"
    MEASURER_COLLECTOR_PORT = 50015

    SIMULATOR_IP = "127.0.0.1"
    SIMULATOR_PORT = 50002

    CLASSIFIER_CONTROLLER_IP = "127.0.0.1"
    CLASSIFIER_CONTROLLER_PORT = 50003

    SERVER_MANAGER_IP = "127.0.0.1"
    SERVER_MANAGER_PORT = 50004

    SFF_CONTROLLER_IP = "127.0.0.1"
    SFF_CONTROLLER_PORT = 50005

    VNF_CONTROLLER_IP = "127.0.0.1"
    VNF_CONTROLLER_PORT = 50006

    NETWORK_CONTROLLER_IP = "127.0.0.1"
    NETWORK_CONTROLLER_PORT = 50007

    P4_CONTROLLER_IP = "127.0.0.1"
    P4_CONTROLLER_PORT = 50007

    DEFINABLE_MEASURER_IP = "127.0.0.1"
    DEFINABLE_MEASURER_PORT = 50008

    ABNORMAL_DETECTOR_IP = "127.0.0.1"
    ABNORMAL_DETECTOR_PORT = 50009

    TURBONET_CONTROLLER_IP = "127.0.0.1"
    TURBONET_CONTROLLER_PORT = 50010

    REGULATOR_IP = "127.0.0.1"
    REGULATOR_PORT = 50011

    SERVER_MANAGER_IP = "127.0.0.1"
    SERVER_MANAGER_PORT = 50012

    SERVER_AGENT_IP = "127.0.0.1"
    SERVER_AGENT_PORT = 50013

    MEDIATOR_IP = "127.0.0.1"
    MEDIATOR_PORT = 50014

    DCN_INFO_RETRIEVER_IP = "127.0.0.1"
    DCN_INFO_RETRIEVER_PORT = 50016

elif TEST_TYPE == TEST_TYPE_INTEGRATE:
    TEST_IP = "127.0.0.1"
    TEST_PORT = 49999

    DASHBOARD_IP = "192.168.99.3"
    DASHBOARD_PORT = 50000

    MEASURER_IP = "192.168.99.2"
    MEASURER_PORT = 50001

    MEASURER_COLLECTOR_IP = "192.168.99.2"
    MEASURER_COLLECTOR_PORT = 50015

    SIMULATOR_IP = "192.168.99.2"
    SIMULATOR_PORT = 50002

    CLASSIFIER_CONTROLLER_IP = "192.168.99.2"
    CLASSIFIER_CONTROLLER_PORT = 50003

    SERVER_MANAGER_IP = "192.168.99.2"
    SERVER_MANAGER_PORT = 50004

    SFF_CONTROLLER_IP = "192.168.99.2"
    SFF_CONTROLLER_PORT = 50005

    VNF_CONTROLLER_IP = "192.168.99.2"
    VNF_CONTROLLER_PORT = 50006

    NETWORK_CONTROLLER_IP = "192.168.99.2"
    NETWORK_CONTROLLER_PORT = 50007

    P4_CONTROLLER_IP = "192.168.99.4"
    # P4_CONTROLLER_IP = "127.0.0.1"
    P4_CONTROLLER_PORT = 50007

    DEFINABLE_MEASURER_IP = "192.168.99.6"
    DEFINABLE_MEASURER_PORT = 50008

    ABNORMAL_DETECTOR_IP = "192.168.99.5"
    # ABNORMAL_DETECTOR_IP = "127.0.0.1"
    ABNORMAL_DETECTOR_PORT = 50009

    TURBONET_CONTROLLER_IP = "192.168.100.5"
    TURBONET_CONTROLLER_PORT = 50010

    REGULATOR_IP = "192.168.99.2"
    REGULATOR_PORT = 50011

    SERVER_MANAGER_IP = "192.168.99.2"
    SERVER_MANAGER_PORT = 50012

    SERVER_AGENT_IP = "192.168.0.194"
    SERVER_AGENT_PORT = 50013

    MEDIATOR_IP = "192.168.99.2"
    MEDIATOR_PORT = 50014

    DCN_INFO_RETRIEVER_IP = "192.168.99.2"
    DCN_INFO_RETRIEVER_PORT = 50016

else:
    raise ValueError("Unknown TEST_TYPE {0}".format(TEST_TYPE))

MIN_PORT = TEST_PORT
MAX_PORT = MEASURER_COLLECTOR_PORT