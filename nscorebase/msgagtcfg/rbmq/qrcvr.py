import sys

if sys.version >= '3':
    import queue as Queue
else:
    import Queue    # type: ignore

import threading
from logging import Logger

import pika

from nscorebase.errorhandler import ExceptionProcessor
from nscorebase.msgagtcfg.cfg import \
    MESSAGE_AGENT_MAX_QUEUE_SIZE
from nscorebase.msgagtcfg.coder import MessageCoder


class QueueReciever(threading.Thread):
    def __init__(self, threadID,
                 rabbitMqServerIP, rabbitMqServerUser, rabbitMqServerPasswd,
                 srcQueueName, msgQueue, logger):
        # type: (int, str, str, str, str, Queue, Logger) -> None
        threading.Thread.__init__(self)
        self.threadID = threadID
        self.rabbitMqServerIP = rabbitMqServerIP
        self.rabbitMqServerUser = rabbitMqServerUser
        self.rabbitMqServerPasswd = rabbitMqServerPasswd
        self.connection = None
        self.channel = None
        self.srcQueueName = srcQueueName
        self.msgQueue = msgQueue
        self.logger = logger

    def run(self):
        self.logger.debug("thread QueueReciever is running.")
        self._recvMsg()

    def _recvMsg(self):
        self._openConnection()
        self._openChannel()
        while True:
            try:
                self.channel.start_consuming()
            except KeyboardInterrupt:
                self._closeChannel()
                self._closeConnection()
                self.logger.info(
                    "msgAgent recv thread get keyboardInterrupt, quit recv().")
                return None
            except Exception as ex:
                ExceptionProcessor(self.logger).log_exception(ex,
                                                             "MessageAgent recvMsg failed")
                self._logChannelStatus()
                self._logConnectionStatus()
                self._closeConnection()
                self._openConnection()
                self._openChannel()

    def _openConnection(self):
        self.logger.info("Opening connection!")
        if not self.connection or self.connection.is_closed:
            self.connection = self._connectRabbitMQServer()

    def _connectRabbitMQServer(self):
        credentials = pika.PlainCredentials(self.rabbitMqServerUser,
                                            self.rabbitMqServerPasswd)
        parameters = pika.ConnectionParameters(self.rabbitMqServerIP,
                                               5672, '/', credentials)
        connection = pika.BlockingConnection(parameters)
        return connection

    def _closeConnection(self):
        self.logger.info("Closing connection!")
        if self.connection.is_open:
            self.connection.close()
            self.connection = None

    def _openChannel(self):
        try:
            if not self.channel or self.channel.is_closed:
                self.channel = self.connection.channel()
            self.channel.queue_declare(queue=self.srcQueueName, durable=True)
            self.channel.basic_consume(queue=self.srcQueueName,
                                       on_message_callback=self.callback)
            self.logger.info(
                ' [*] _openChannel(): Waiting for messages. To exit press CTRL+C')
        except Exception as ex:
            ExceptionProcessor(self.logger).log_exception(ex,
                                                         "Open channel failed!")

    def _closeChannel(self):
        if self.channel.is_open:
            self.logger.info('Channel is running! Close it.')
            requeued_messages = self.channel.cancel()
            self.channel.stop_consuming()
        elif self.channel.is_closed:
            self.logger.info("Channel has already been closed!")
        else:
            self.logger.info("Channel is closing!")

    def _logChannelStatus(self):
        if self.channel.is_open:
            self.logger.info('Channel is running! Close it.')
        elif self.channel.is_closed:
            self.logger.info("Channel has already been closed!")
        else:
            self.logger.info("Channel is closing!")

    def _logConnectionStatus(self):
        if self.connection.is_open:
            self.logger.info("Connection is opened!")
        elif self.connection.is_closed:
            self.logger.info("Connection is closed!")
        else:
            self.logger.info("Unknown connection status.")

    def callback(self, ch, method, properties, body):
        self.logger.debug(" [x] Received ")
        # t1 = time.time()
        # body = MessageCoder.decodeMessage(body)
        # t2 = time.time()
        # self.logger.debug("recv d1 {0}".format(t2-t1))
        if self.msgQueue.qsize() < MESSAGE_AGENT_MAX_QUEUE_SIZE:
            self.msgQueue.put(body)
        else:
            raise ValueError("MessageAgent recv qeueu full! Drop new msg!")
        self.logger.debug(" [x] Done")
        ch.basic_ack(delivery_tag=method.delivery_tag)
