from json.encoder import INFINITY
import sys

from nscorebase.msgagtcfg.cfg import MESSAGE_AGENT_RABBIT_QUEUE_RETRY_WAIT_TIME

if sys.version >= '3':
    import queue as Queue
else:
    import Queue    # type: ignore

import json
import time

import pika

import nscorebase
from nscorebase.errorhandler import ExceptionProcessor
from nscorebase.msgagtcfg.agtbase import MessageAgentBase
from nscorebase.msgagtcfg.coder import MessageCoder
from nscorebase.msgagtcfg.rbmq.qrcvr import QueueReciever


class RabbitMQAgent(MessageAgentBase):
    def __init__(self, *args, **kwargs):
        super(RabbitMQAgent, self).__init__(*args, **kwargs)
        self.readRabbitMQConf()
        self._threadSet = {}
        self._publisherConnection = None

    def readRabbitMQConf(self):
        filepath = nscorebase.__file__.split("__init__.py")[0] + 'base/rabbitMQConf.json'
        with open(filepath, 'r') as jsonfile:
            json_string = json.load(jsonfile)
            self.rabbitMqServerIP = str(json_string["RABBITMQSERVERIP"])
            self.rabbitMqServerUser = str(json_string["RABBITMQSERVERUSER"])
            self.rabbitMqServerPasswd = str(
                json_string["RABBITMQSERVERPASSWD"])
            self.logger.info(
                "rabbimqConf -> Server:{0}, User:{1}, Passwd:{2}\n".format(
                    self.rabbitMqServerIP, self.rabbitMqServerUser,
                    self.rabbitMqServerPasswd))

    def setRabbitMqServer(self, serverIP, serverUser, serverPasswd):
        self.rabbitMqServerIP = serverIP
        self.rabbitMqServerUser = serverUser
        self.rabbitMqServerPasswd = serverPasswd

    def genQueueName(self, queueType, zoneName=""):
        if zoneName == "":
            return queueType
        else:
            return queueType + "_" + zoneName

    def sendMsg(self, dstQueueName, message, maxRetryNum=INFINITY):
        self.logger.debug("MessageAgent ready to send msg")
        sendSucc = False
        cnt = 0
        while not sendSucc:
            try:
                self._publisherConnection = self._connectRabbitMQServer()
                if self._publisherConnection.is_open:
                    channel = self._publisherConnection.channel()
                    if channel.is_open:
                        channel.queue_declare(queue=dstQueueName, durable=True)
                        channel.basic_publish(exchange='', routing_key=dstQueueName,
                                            body=MessageCoder.encodeMessage(
                                                message),
                                            properties=pika.BasicProperties(
                                                delivery_mode=2)
                                            # make message persistent
                                            )
                        self.logger.debug(" [x] Sent ")
                        channel.close()
                        sendSucc = True
                        break
                    else:
                        self.logger.warning("Rabbitmq channel is not open!")
                else:
                    self.logger.warning("Rabbitmq connection is not open!")
            except Exception as ex:
                ExceptionProcessor(self.logger).log_exception(ex,
                                                            "MessageAgent sendMsg failed")
            finally:
                if self._publisherConnection.is_open:
                    self._publisherConnection.close()
                if not sendSucc:
                    cnt += 1
                    if cnt > maxRetryNum:
                        break
                    self.logger.info(
                        "retry for the {0}/{1} times".format(cnt, maxRetryNum))
                    time.sleep(MESSAGE_AGENT_RABBIT_QUEUE_RETRY_WAIT_TIME)

    def startRecvMsg(self, srcQueueName):
        self.logger.debug(
            "MessageAgent.startRecvMsg() on queue {0}".format(srcQueueName))
        if srcQueueName in self.msgQueues:
            self.logger.warning("Already listening on recv queue.")
        else:
            self.msgQueues[srcQueueName] = Queue.Queue()
            try:
                # start a new thread to recieve
                thread = QueueReciever(len(self._threadSet),
                                       self.rabbitMqServerIP, self.rabbitMqServerUser, self.rabbitMqServerPasswd,
                                       srcQueueName, self.msgQueues[srcQueueName], self.logger)
                self._threadSet[srcQueueName] = thread
                thread.setDaemon(True)
                thread.start()
                result = True
            except:
                self.logger.error("MessageAgent startRecvMsg failed")
                result = False
            finally:
                return result

    def _connectRabbitMQServer(self):
        credentials = pika.PlainCredentials(self.rabbitMqServerUser,
                                            self.rabbitMqServerPasswd)
        parameters = pika.ConnectionParameters(self.rabbitMqServerIP,
                                               5672, '/', credentials)
        connection = pika.BlockingConnection(parameters)
        return connection

    def deleteQueue(self, queueName):
        while True:
            try:
                self._publisherConnection = self._connectRabbitMQServer()
                channel = self._publisherConnection.channel()
                channel.queue_purge(queue=queueName)
                self.logger.debug(
                    " Delete queueu {0} successfully!".format(queueName))
                channel.close()
            except Exception as ex:
                ExceptionProcessor(self.logger).log_exception(ex,
                                                             "MessageAgent delete queue failed temporally. Retry")
            finally:
                if self._publisherConnection.is_open:
                    self._publisherConnection.close()
                break

    def _disConnectRabbiMQServer(self):
        if self._publisherConnection != None:
            if self._publisherConnection.is_open:
                self._publisherConnection.close()