#!/usr/bin/python
# -*- coding: UTF-8 -*-

from nscorebase.rbmqcfg import RabbitMQSetter
from nscorebase.test.testConfig import TEST_TYPE, TEST_TYPE_INTEGRATE, TEST_TYPE_LOCAL

RABBITMQ_SERVER_USER = "mq"
RABBITMQ_SERVER_PASSWORD = "123456"

if TEST_TYPE == TEST_TYPE_LOCAL:
    RABBITMQ_SERVER_IP = "127.0.0.1"
elif TEST_TYPE == TEST_TYPE_INTEGRATE:
    RABBITMQ_SERVER_IP = "192.168.99.2"
else:
    raise ValueError("Unknown TEST_TYPE {0}".format(TEST_TYPE))

rmqs = RabbitMQSetter()
rmqs.setRabbitMQConf(rabbitMqServerIP=RABBITMQ_SERVER_IP,
                     rabbitMqServerUser=RABBITMQ_SERVER_USER,
                     rabbitMqServerPasswd=RABBITMQ_SERVER_PASSWORD)
