from nscorebase.pickleIO import PickleIO


class MessageCoder(object):
    @staticmethod
    def encodeMessage(message):
        return PickleIO().obj_to_pickle(message, b64=True)

    @staticmethod
    def decodeMessage(message):
        return PickleIO().pickle_to_obj(message, b64=True)