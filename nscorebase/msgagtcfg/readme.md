# MessageAgent Usage

Usage:
    from nscorebase.messageAgent import SAMMessage, MessageAgent, TURBONET_ZONE, \
                                        REGULATOR_QUEUE, 
                                        DEFINABLE_MEASURER_QUEUE, ABNORMAL_DETECTOR_QUEUE

Use case 1 - abnormal detector:
    # Thread 1 (main)
    mA = MessageAgent()
    mA.startMsgReceiverRPCServer(ABNORMAL_DETECTOR_IP, ABNORMAL_DETECTOR_PORT)
    # send Request to definable measurer to get datacenter infomation
    while True:
        time.sleep(3)
        msgType = MSG_TYPE_REQUEST
        request = Request(0, uuid.uuid1(), REQUEST_TYPE_GET_DCN_INFO)
        msg = SAMMessage(msgType, request)
        mA.sendMsgByRPC(DEFINABLE_MEASURER_IP, DEFINABLE_MEASURER_PORT, msg)

    # Thread 2
    # recv message in a While loop from a grpc socket: 
    while True:
        msg = mA.getMsgByRPC(ABNORMAL_DETECTOR_IP, ABNORMAL_DETECTOR_PORT)
        msgType = msg.getMessageType() # Maybe reply from definable measurer
        source = msg.getSource()
        if msgType == MSG_TYPE_REPLY:
            pass
        else:
            pass

    # send "abnormal handle command" to REGULATOR_QUEUE (PS: regulator can handle abnormal and failure)
    queueName = REGULATOR_QUEUE
    mA = MessageAgent()
    msgType = MSG_TYPE_ABNORMAL_DETECTOR_CMD
    attr = None # Store all abnormal and failure in this variable
    cmd = Command(CMD_TYPE_HANDLE_FAILURE_ABNORMAL, uuid.uuid1(), attributes={attr})
    msg = SAMMessage(msgType, cmd)
    mA.sendMsg(queueName, msg)

Use case 2 - definable measurer:
    # Thread 1 (main)
    mA = MessageAgent()
    mA.startMsgReceiverRPCServer(DEFINABLE_MEASURER_IP, DEFINABLE_MEASURER_PORT)
    # send Request to measurer to get datacenter infomation
    while True:
        time.sleep(3)
        msgType = MSG_TYPE_REQUEST
        request = Request(0, uuid.uuid1(), REQUEST_TYPE_GET_DCN_INFO)
        msg = SAMMessage(msgType, request)
        mA.sendMsgByRPC(MEASURER_IP, MEASURER_PORT, msg)

    # Thread 2
    # recv message in a While loop from a grpc socket: 
    while True:
        msg = mA.getMsgByRPC(DEFINABLE_MEASURER_IP, DEFINABLE_MEASURER_PORT)
        msgType = msg.getMessageType() # Maybe request from abnormal detector, or reply (dcn info) from measurer
        source = msg.getSource()
        if msgType == MSG_TYPE_REPLY:
            pass
            # from chen hao
            reply = msg.getbody()   #  type: CommandReply
            attributes = reply.attributes
        elif msgType == MSG_TYPE_REQUEST:
            request = msg.getbody()
            rq_type = request.rq_type
            if rq_type == REQUEST_TYPE_GET_DCN_INFO:
                # from wang xuan
                rply = Reply()
                rply.attributes = {"server":servers, "link":links, ...}
            elif rq_type == REQUEST_TYPE_GET_LINK_INFO:
                # from  chen hao
                rply = Reply()
                rply.attributes = {"links":links}
            else:
                # unknown
                pass
            # construct reply message, e.g. rplyMsg
            rplyMsg = SAMMessage(MSG_TYPE_REPLY, rply)
            # send rplyMsg by the following code
            mA.sendMsgByRPC(source['srcIP'], source['srcPort'], rplyMsg)
        else:
            pass

    # send message to abnormal detector
    attributes = self.getTopoAttributes()
    rply = Reply(request.rq_id,
        REQUEST_STATE_SUCCESSFUL, attributes)
    msg = SAMMessage(msgType, reply)
    mA.sendMsgByRPC(ABNORMAL_DETECTOR_IP, ABNORMAL_DETECTOR_PORT, msg)

Use case 3 - elastic orchestrator(regulator):
    # recv message in a While loop from REGULATOR_QUEUE
    queueName = REGULATOR_QUEUE
    mA.startRecvMsg(queueName)
    while True:
        msg = mA.getMsg(queueName)

TODO:
future works: async version of MessageAgent
    https://grpc.github.io/grpc/python/grpc_asyncio.html
    https://www.liaoxuefeng.com/wiki/1016959663602400/1017968846697824
    https://stackoverflow.com/questions/38387443/how-to-implement-a-async-grpc-python-server
    https://zhuanlan.zhihu.com/p/349832978
    https://docs.python.org/3.9/library/asyncio.html
    https://docs.python.org/3.9/library/asyncio-task.html#awaitables