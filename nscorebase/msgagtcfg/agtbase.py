import sys

from nscorebase.msgagtcfg.coder import MessageCoder

if sys.version >= '3':
    import queue as Queue
else:
    import Queue    # type: ignore

import ctypes
import inspect
import time
from logging import Logger
from multiprocessing import Manager
# from multiprocessing.context import BaseContext
from typing import Dict, Union

from nscorebase.command import Command, CommandReply
from nscorebase.logcfg import LoggerConfigurator
from nscorebase.msgagtcfg.msg import SAMMessage
from nscorebase.multiprocess.mpmgr import MultiprocessManager
from nscorebase.request import Reply, Request


class MessageAgentBase(object):
    def __init__(self,
                 logger=None,    # type: Union[Logger, None]
                 maxSenderNum=1,    # type: int
                 context=None,  # type: Union[BaseContext, None]
                 manager=None   # type: Union[Manager, None]
                 ):
        if logger != None:
            self.logger = logger
        else:
            # Can't run file logger when call __del__() methods
            # self.log_cfgr = LoggerConfigurator(__name__, './log',
            #     'messageAgent.log', level='info')
            self.log_cfgr = LoggerConfigurator(__name__, None,
                                                  None, level='info')
            self.logger = self.log_cfgr.get_logger()
        self.logger.info("Init MessaageAgent.")

        self.msgQueues = {}  # type: Dict[str, Queue.Queue]
        self.maxSenderNum = maxSenderNum

        if sys.version < '3':
            self.logger.error("Can't support multi-sender in python2.")
            assert self.maxSenderNum == 1
        assert self.maxSenderNum >= 1
        if self.maxSenderNum > 1:
            if manager == None:
                self.logger.error(
                    "Must provide multiprocessing Manger for parallel sender.")
                assert manager != None
                # self.manager = Manager()
            else:
                self.manager = manager
            self.ctx = context
            self.mm = MultiprocessManager(context = self.ctx, manager = self.manager)
            self.enableMultiprocess = True
        else:
            self.enableMultiprocess = False

    def isCommand(self, body):
        return isinstance(body, Command)

    def isCommandReply(self, body):
        return isinstance(body, CommandReply)

    def isRequest(self, body):
        return isinstance(body, Request)

    def isReply(self, body):
        return isinstance(body, Reply)

    def getMsg(self, srcQueueName, block=False, timeout=None, throughput=1000):
        msg = None
        if srcQueueName in self.msgQueues:
            if not self.msgQueues[srcQueueName].empty():
                msg = self.msgQueues[srcQueueName].get(block=block, timeout=timeout)
                # self.logger.debug("msg {0}".format(msg))
                msg = MessageCoder.decodeMessage(msg)
            else:
                msg = SAMMessage(None, None)
        else:
            self.logger.error(
                "No such msg queue. QueueName:{0}".format(srcQueueName))
            msg = SAMMessage(None, None)
        if block == False:
            time.sleep(1/float(throughput))    # throughput pps msg
        return msg

    def getMsgCnt(self, srcQueueName):
        if srcQueueName in self.msgQueues:
            return self.msgQueues[srcQueueName].qsize()
        else:
            self.logger.error(
                "No such msg queue. QueueName:{0}".format(srcQueueName))
            return -1

    def _async_raise(self, tid, exctype):
        """raises the exception, performs cleanup if needed"""
        tid = ctypes.c_long(tid)
        if not inspect.isclass(exctype):
            exctype = type(exctype)
        res = ctypes.pythonapi.PyThreadState_SetAsyncExc(tid,
                                                         ctypes.py_object(exctype))
        if res == 0:
            raise ValueError("Invalid thread id")
        elif res != 1:
            # """if it returns a number greater than one, you're in trouble,
            # and you should call it again with exc=NULL to revert the effect"""
            ctypes.pythonapi.PyThreadState_SetAsyncExc(tid, None)
            raise SystemError("PyThreadState_SetAsyncExc failed")
