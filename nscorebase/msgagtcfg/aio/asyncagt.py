#!/usr/bin/python
# -*- coding: UTF-8 -*-

import sys

from packaging import version

from nscorebase.logcfg import LoggerConfigurator
from nscorebase.msgagtcfg.grpc.aio.asyncgrpc import \
    AsyncGrpcAgent
from nscorebase.msgagtcfg.const import *
from nscorebase.msgagtcfg.rbmq.agt import RabbitMQAgent


class AsyncMessageAgent(AsyncGrpcAgent, RabbitMQAgent):
    def __init__(self, *args, **kwargs):
        super(AsyncMessageAgent, self).__init__(*args, **kwargs)
        raise ValueError("Haven't implement AsyncMessageAgent successfull.")

    def __del__(self):
        self.log_cfgr = LoggerConfigurator(__name__, None,
                                              None, level='info')
        self.logger = self.log_cfgr.get_logger()
        self.logger.info("Delete MessageAgent.")
        for srcQueueName, thread in self._threadSet.items():
            self.logger.debug("check thread is alive?")
            if version.parse(sys.version.split(' ')[0]) \
                    >= version.parse('3.9'):
                threadLiveness = thread.is_alive()
            else:
                threadLiveness = thread.isAlive()
            if threadLiveness:
                self.logger.info("Kill thread: %d" % thread.ident)
                self._async_raise(thread.ident, KeyboardInterrupt)
                thread.join()

        self.logger.info("Disconnect from RabbiMQServer.")
        self._disConnectRabbiMQServer()
