import uuid


class SAMMessage(object):
    def __init__(self, msgType, body):
        self._msgType = msgType  # can not be a type()
        self._msgID = uuid.uuid1()
        self._body = body
        self._source = None

    def getMessageType(self):
        return self._msgType

    def getMessageID(self):
        return self._msgID

    def getbody(self):
        return self._body

    def setSource(self, source):
        self._source = source

    def getSource(self):
        return self._source

    def __str__(self):
        output = "Message type is {0} ".format(self._msgType)\
            + "Message ID is {0} ".format(self._msgID)\
            + "Message body is {0}".format(self._body)
        return output
