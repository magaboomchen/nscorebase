'''
TODO: There may be a bug in LoggerConfigurator
It will consume a lot of memory!
I suspect the handler is the root cause.
'''

import logging
import multiprocessing
import os
import random
from logging import handlers


class LoggerConfigurator(object):
    level_relations = {
        'debug': logging.DEBUG,
        'info': logging.INFO,
        'warning': logging.WARNING,
        'error': logging.ERROR,
        'crit': logging.CRITICAL
    }

    def __init__(self, logger_name, directory=None, filename=None,
                 level='info', fmt=None, when='D', interval=1,
                 back_cnt=7, rm_handler=True, screen_print=True,
                 file_log=True, multiprocess=False):
        self.th = None
        self.log_filepath = None
        self.sh = None
        self.directory = directory
        if directory is not None and not os.path.exists(directory):
            try:
                os.mkdir(directory)
            except OSError as ex:
                template = "An exception of type {0} occurred. Arguments:\n{1!r}"
                message = template.format(type(ex).__name__, ex.args)
                logging.error("Logger occurs error: {0}".format(message))

        self.logger_name = logger_name
        self._append_logger_name_suffix()

        if multiprocess:
            self.logger = multiprocessing.get_logger()
        else:
            self.logger = logging.getLogger(self.logger_name)
        logging_level = self.level_relations.get(level)
        self.logger.setLevel(logging_level)

        if fmt is None:
            fmt = '%(asctime)s - %(filename)s[line:%(lineno)d]- %(levelname)s:\t%(message)s'
        self.format_str = logging.Formatter(fmt)

        if screen_print:
            self.rm_handler = rm_handler
            self.add_stream_handler()

        if file_log:
            self.filename = filename
            # self._append_file_name_suffix()
            self.add_file_handler(when, interval, back_cnt)

    def add_stream_handler(self):
        self.sh = logging.StreamHandler()  # print to screen
        self.sh.setFormatter(self.format_str)
        self.logger.addHandler(self.sh)
        if self.rm_handler:
            self.sh.close()

    def add_file_handler(self, when, interval, back_cnt):
        if self.directory is not None and self.filename is not None:
            self.log_filepath = self.directory + '/' + self.filename
            self.th = handlers.TimedRotatingFileHandler(filename=self.log_filepath,
                                                        when=when, interval=interval,
                                                        backupCount=back_cnt,
                                                        encoding='utf-8')
            # backupCount: the max log file number
            # when: is the unit of interval
            # S second
            # M Minute
            # H hour
            # D day
            # W week
            # midnight
            self.th.setFormatter(self.format_str)
            self.logger.addHandler(self.th)
            if self.rm_handler:
                self.th.close()

    def get_logger(self):
        return self.logger

    def get_file_handler(self):
        return self.th

    def get_screen_handler(self):
        return self.sh

    def _append_logger_name_suffix(self):
        suffix = self._get_rnd_string(5)
        self.logger_name = self.logger_name + "_" + suffix

    def _append_file_name_suffix(self):
        suffix = self._get_rnd_string(5)
        self.filename = self.filename + "._" + suffix

    def _get_rnd_string(self, str_len):
        return ''.join(random.sample(['z', 'y', 'x', 'w', 'v', 'u', 't', 's',
                                      'r', 'q', 'p', 'o', 'n', 'm', 'l', 'k',
                                      'j', 'i', 'h', 'g', 'f', 'e', 'd', 'c',
                                      'b', 'a'],
                                     str_len))
