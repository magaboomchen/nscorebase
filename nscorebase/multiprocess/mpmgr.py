import multiprocessing as mp
import sys
import time
from multiprocessing import Process
from multiprocessing.context import BaseContext
from multiprocessing.managers import BaseManager
from typing import Any, Callable, List

from nscorebase.logcfg import LoggerConfigurator

PROCESS_STARTUP_WAITING_TIME = 0


class MultiprocessManager(object):
    def __init__(self, context: BaseContext = None, manager: BaseManager = None) -> None:
        log_cfgr = LoggerConfigurator(__name__, './log',
                                      'mpmgr.log',
                                      level='debug')
        self.logger = log_cfgr.get_logger()

        self.process_list = []  # type: List[Process]
        if sys.version > '3':
            if context is None:
                self.ctx = mp.get_context('forkserver')
            else:
                self.ctx = context
        else:
            self.ctx = mp
        if manager is None:
            self.manager = mp.Manager()
        else:
            self.manager = manager

    def start_process(self, callable_obj: Callable, name: str = None, *args: Any, **kwargs: Any) -> int:
        self.logger.info("args {0}".format(args))
        self.logger.info("kwargs {0}".format(kwargs))
        retry_num = 0
        while True:
            process = self.ctx.Process(target=callable_obj, name=name, args=args, kwargs=kwargs)
            self.logger.info("try to start process {0}".format(process.name))
            process.start()
            time.sleep(PROCESS_STARTUP_WAITING_TIME + retry_num)
            if process.is_alive():
                self.logger.info("start process {0} successfully.".format(process.name))
                self.process_list.append(process)
                break
            else:
                self.logger.error("start process {0} failed!".format(process.name))
                process.close()
                retry_num += 1
        return process.pid

    def wait_all_process(self):
        self.logger.info("wait all process join")
        # while True:
        #     active_p_num = self.getActiveProcessNum()
        #     if active_p_num > 0:
        #         time.sleep(1)
        #     else:
        #         break
        for process in self.process_list:
            try:
                if process.is_alive():
                    process.join()
                # print("pname:{0}, live?{1}".format(process.name, process.is_alive()))
            except ValueError as ex:
                self.logger.debug(ex)

    def get_active_process_num(self):
        cnt = 0
        for process in self.process_list:
            if process.is_alive():
                cnt += 1
        return cnt

    def __del__(self):
        self.log_cfgr = LoggerConfigurator(__name__, None,
                                           None, level='info')
        self.logger = self.log_cfgr.get_logger()
        self.logger.info("Delete MultiprocessManager.")
        for process in self.process_list:
            if process.is_alive():
                process.terminate()
                process.join()
                process.close()
            else:
                self.logger.warning("process {0} is not alive.".format(process.pid))
