from nscorebase.logcfg import LoggerConfigurator


class ArgParserBase(object):
    def __init__(self):
        self.args = None
        log_cfgr = LoggerConfigurator(__name__, './log',
                                      'ArgParserBase.log', level='info')
        self.logger = log_cfgr.get_logger()

    def get_args(self):
        return self.args.__dict__

    def log_args(self):
        self.logger.info("argparse.args=", self.args, type(self.args))
        d = self.args.__dict__
        for key, value in d.iteritems():
            self.logger.info('%s = %s' % (key, value))
