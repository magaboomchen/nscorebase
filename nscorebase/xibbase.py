from nscorebase.pickleIO import PickleIO


class XInfoBase(object):
    def __init__(self):
        self.dbA = None
        self.pIO = PickleIO()

    def addDatabaseAgent(self, host, user, passwd):
        from nscorebase.dbagent import DatabaseAgent
        if host != None and user != None and passwd != None:
            self.dbA = DatabaseAgent(host, user, passwd)
        else:
            self.dbA = DatabaseAgent()

    def genAvailableMiniNum4List(self, numList):
        if numList == []:
            return 0
        numList.sort()
        maxNum = max(numList)
        minNum = min(numList)
        if minNum != 0:
            return 0
        for i in range(len(numList)-1):
            currentNum = numList[i]
            nextNum = numList[i+1]
            if nextNum-currentNum > 1:
                return currentNum + 1
        else:
            return maxNum + 1
