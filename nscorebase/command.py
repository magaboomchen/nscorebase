from typing import Any, Dict, Union
from uuid import UUID

CMD_STATE_WAITING = "CMD_STATE_WAITING"
CMD_STATE_PROCESSING = "CMD_STATE_PROCESSING"
CMD_STATE_SUCCESSFUL = "CMD_STATE_SUCCESSFUL"
CMD_STATE_FAIL = "CMD_STATE_FAIL"

CMD_TYPE_ADD_SFC = "CMD_TYPE_ADD_SFC"


class Command(object):
    def __init__(self, cmd_type: Any, cmd_id: UUID, attributes: Dict[str, Any] = None) -> None:
        self.cmd_type = cmd_type
        self.cmd_id = cmd_id
        if attributes is None:
            self.attributes = {}
        else:
            self.attributes = attributes

    def __str__(self):
        string = "{0}\n".format(self.__class__)
        for key, values in self.__dict__.items():
            string = string + "{0}:{1}\n".format(key, values)
        return string

    def __repr__(self):
        return str(self)


class CommandReply(object):
    def __init__(self, cmd_id: UUID, cmd_state: str,
                 attributes: Dict[str, Any] = None) -> None:
        self.cmd_id = cmd_id
        self.cmd_state = cmd_state
        if attributes is None:
            self.attributes = {}
        else:
            self.attributes = attributes

    def __str__(self):
        string = "{0}\n".format(self.__class__)
        for key, values in self.__dict__.items():
            string = string + "{0}:{1}\n".format(key, values)
        return string

    def __repr__(self):
        return str(self)
