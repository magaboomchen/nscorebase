'''
compatibility
https://rebeccabilbro.github.io/convert-py2-pickles-to-py3/
'''

import base64
import sys

if sys.version > '3':
    import _pickle as cPickle
else:
    import cPickle


class PickleIO(object):
    def __init__(self):
        self.defulatPickleProtocol = 2

    def rd_pickle_file(self, filepath):
        df = open(filepath, 'rb')
        if sys.version > '3':
            obj = cPickle.load(df, encoding="latin1")
        else:
            obj = cPickle.load(df)
        df.close()
        return obj

    def wt_pickle_file(self, filepath, obj):
        df = open(filepath, 'wb')
        cPickle.dump(obj, df, protocol=self.defulatPickleProtocol)
        df.close()

    def obj_to_pickle(self, obj, b64=True):
        pkl = cPickle.dumps(obj,protocol=self.defulatPickleProtocol)
        if b64:
            pkl = base64.b64encode(pkl)
        return pkl

    def pickle_to_obj(self, pkl_inst, b64: bool = True):
        if b64:
            pkl_inst = base64.b64decode(pkl_inst)
        if sys.version > '3':
            return cPickle.loads(pkl_inst, encoding="latin1")
        else:
            return cPickle.loads(pkl_inst)
