import traceback
from logging import Logger


class ExceptionProcessor(object):
    def __init__(self, logger: Logger) -> None:
        self.logger = logger

    def log_exception(self, ex, note=None):
        template = "An exception of type {0} occurred. Arguments: {1!r}"
        message = template.format(type(ex).__name__, ex.args)
        self.logger.error(
            "{0}: \n"
            "ex.message:\t{1}\n" \
            "traceback.format_exc():\t{2}".format(
                note,
                message,
                traceback.format_exc()
            )
        )