from nscorebase.command import (CMD_STATE_FAIL, CMD_STATE_PROCESSING,
                                CMD_STATE_SUCCESSFUL, CMD_STATE_WAITING)
from nscorebase.xibbase import XInfoBase


class CommandBase(XInfoBase):
    def __init__(self):
        super(CommandBase, self).__init__()
        self._commandsInfo = {}

    def add_cmd(self, cmd):
        self._commandsInfo[cmd.cmd_id] = {
            'cmd': cmd,
            'state': CMD_STATE_WAITING,
            'cmdReply': None,
            'parentCmdID': None,
            'childCmdID': {}  # {CmdName1:cmd_id,CmdName1:cmd_id}
        }

    def get_cmd(self, cmd_id):
        if cmd_id in self._commandsInfo:
            return self._commandsInfo[cmd_id]['cmd']
        else:
            return None

    def has_cmd(self, cmd_id):
        if cmd_id in self._commandsInfo:
            return True
        else:
            return False

    def del_cmd_with_child_cmd(self, cmd_id):
        for key, childCmdID in self._commandsInfo[cmd_id]['childCmdID'].items():
            del self._commandsInfo[childCmdID]
        del self._commandsInfo[cmd_id]

    def add_child_cmd_to_cmd(self, currentCmdID, childCmdName, childCmdID):
        self._commandsInfo[currentCmdID]['childCmdID'][childCmdName] = childCmdID

    def delChildCmd4Cmd(self, currentCmdID, childCmdName):
        del self._commandsInfo[currentCmdID]['childCmdID'][childCmdName]

    def addParentCmd2Cmd(self, currentCmdID, parentCmdID):
        self._commandsInfo[currentCmdID]['parentCmdID'] = parentCmdID

    def getCmdState(self, cmd_id):
        return self._commandsInfo[cmd_id]['state']

    def changeCmdState(self, cmd_id, state):
        self._commandsInfo[cmd_id]['state'] = state

    def transitCmdState(self, cmd_id, statePrev, stateNext):
        if self.has_cmd(cmd_id):
            state = self._commandsInfo[cmd_id]['state']
            if state == statePrev:
                self._commandsInfo[cmd_id]['state'] = stateNext

    def getChildCmdState(self, cmd_id, childCmdName):
        cCmdID = self._commandsInfo[cmd_id]['childCmdID'][childCmdName]
        return self._commandsInfo[cCmdID]['state']

    def getChildCmd(self, cmd_id, childCmdName):
        cCmdID = self._commandsInfo[cmd_id]['childCmdID'][childCmdName]
        return self._commandsInfo[cCmdID]['cmd']

    def getCmdType(self, cmd_id):
        return self._commandsInfo[cmd_id]['cmd'].cmd_type

    def getParentCmdID(self, cmd_id):
        return self._commandsInfo[cmd_id]['parentCmdID']

    def addCmdRply(self, cmd_id, cmdRply):
        self._commandsInfo[cmd_id]['cmdReply'] = cmdRply

    def isChildCmdHasCmdRply(self, cmd_id):
        if cmd_id not in self._commandsInfo.keys():
            raise ValueError("Unknown cmd_id {0}".format(cmd_id))
        if self._commandsInfo[cmd_id]['cmdReply'] is not None:
            return True
        else:
            return False

    def getChildCMdRplyList(self, parentCmdID):
        childCmdRplyList = []
        cmdInfo = self._commandsInfo[parentCmdID]
        for key, childCmdID in cmdInfo['childCmdID'].items():
            childCmdRply = self._commandsInfo[childCmdID]['cmdReply']
            childCmdRplyList.append(childCmdRply)
        return childCmdRplyList

    def isParentCmdSuccessful(self, cmd_id):
        # if all child cmd is successful, then send cmdRply
        cmdInfo = self._commandsInfo[cmd_id]
        for key, childCmdID in cmdInfo['childCmdID'].items():
            if self.getCmdState(childCmdID) != CMD_STATE_SUCCESSFUL:
                return False
        return True

    def isParentCmdFailed(self, cmd_id):
        # if at least one child cmd is failed, then parent cmd failed
        cmdInfo = self._commandsInfo[cmd_id]
        for key, childCmdID in cmdInfo['childCmdID'].items():
            if self.getCmdState(childCmdID) == CMD_STATE_FAIL:
                return True
        return False

    def isOnlyOneChildCmdFailed(self, cmd_id):
        cmdInfo = self._commandsInfo[cmd_id]
        count = 0
        for key, childCmdID in cmdInfo['childCmdID'].items():
            if self.getCmdState(childCmdID) == CMD_STATE_FAIL:
                count = count + 1
        if count == 1:
            return True
        else:
            return False

    def isAllChildCmdDetermined(self, cmd_id):
        cmdInfo = self._commandsInfo[cmd_id]
        for key, childCmdID in cmdInfo['childCmdID'].items():
            if self.getCmdState(childCmdID) == CMD_STATE_WAITING or \
                    self.getCmdState(childCmdID) == CMD_STATE_PROCESSING:
                return False
        return True

    def isParentCmdWaiting(self, cmd_id):
        cmdInfo = self._commandsInfo[cmd_id]
        for key, childCmdID in cmdInfo['childCmdID'].items():
            if self.getCmdState(childCmdID) == CMD_STATE_WAITING:
                return True
        return False

    def __str__(self):
        string = "{0}\n".format(self.__class__)
        for key, values in self.__dict__.items():
            string = string + "{0}:{1}\n".format(key, values)
        return string

    def __repr__(self):
        return str(self)
